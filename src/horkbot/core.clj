 (ns horkbot.core
   (:require [horkbot.config :as config]
             [horkbot.util :as util]
             [horkbot.evaluator :as evaluator]
             [clojure.core.async :as async :refer [>! <! go go-loop]])
   (:import java.lang.Thread)
  (:gen-class))

;; https://gnurdle-hackers.slack.com/api/channels.history?_x_id=3abc856c-1502592351.089
  ;; 3abc856c might be the channel?
  ;; -* is timestamp
;; $.get("api/channels.history?_x_id=3abc856c-1502592448.059", {}, function(data) {alert("boogers:" + data)})

;; Direct msg to bot:
;; {:type message, :channel D2S6C946B, :user U1MQW7PB9, :text howdy, :ts 1502496968.437132, :source_team T1CFGJLMT, :team T1CFGJLMT}
;;
;; User logs on:
;; {:type presence_change, :presence active, :user U1MQW7PB9}
;;
;; User logs off:
;; {:type presence_change, :presence away, :user U1MQW7PB9}
;;
;; Users
;; U1MQW7PB9 : jdtaylor
;; U1CFLAB54 : ?
;; U37LLRDPT
;; U64H99EUW
;; U1CF6DMQU

;; jdt: CRASH after running for weeks:
;; ping? pending pings: 6
;; incoming: {:type error, :error {:msg Socket URL has expired, :code 1}}
;; ping? pending pings: 0
;; Exception in thread "async-dispatch-13" org.eclipse.jetty.websocket.api.WebSocketException: RemoteEndpoint unavailable, current state [CLOSED], expecting [OPEN or CONNECTED]
;;  at org.eclipse.jetty.websocket.common.WebSocketSession.getRemote(WebSocketSession.java:263)
;;  at gniazdo.core$connect_with_client$reify__11455.send_msg(core.clj:135)
;;  at clj_slackbot.comms.slack_rtm$connect_socket$fn__11484$state_machine__5773__auto____11485$fn__11487.invoke(slack_rtm.clj:34)
;;  at clj_slackbot.comms.slack_rtm$connect_socket$fn__11484$state_machine__5773__auto____11485.invoke(slack_rtm.clj:31)
;;  at clojure.core.async.impl.ioc_macros$run_state_machine.invokeStatic(ioc_macros.clj:940)
;;  at clojure.core.async.impl.ioc_macros$run_state_machine.invoke(ioc_macros.clj:939)
;;  at clojure.core.async.impl.ioc_macros$run_state_machine_wrapped.invokeStatic(ioc_macros.clj:944)
;;  at clojure.core.async.impl.ioc_macros$run_state_machine_wrapped.invoke(ioc_macros.clj:942)
;;  at clojure.core.async.impl.ioc_macros$take_BANG_$fn__5789.invoke(ioc_macros.clj:953)
;;  at clojure.core.async.impl.channels.ManyToManyChannel$fn__1448.invoke(channels.clj:133)
;;  at clojure.lang.AFn.run(AFn.java:22)
;;  at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1145)
;;  at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:615)
;;  at java.lang.Thread.run(Thread.java:745)
;; 

 (defn make-comm [id config]
   (let [f (util/kw->fn id)]
     (f config)))

 (defn -main [& args]
  (let [config (config/read-config)
        inst-comm (fn []
                    (println ":: building com:" (:comm config))
                    (make-comm (:comm config) config))]
    (println ":: starting with config:" config)

    (go-loop [[in out stop] (inst-comm)]
      (println ":: waiting for input")
      (if-let [form (<! in)]
        (let [input (:input form)
        ;      res (evaluator/eval-expr input)
              ]
          (println ":: form >> " input)
        ;  (println ":: => " res)
        ;  (>! out (assoc form :evaluator/result res))
          (recur [in out stop]))

        ;; something wrong happened, re init
        (do
          (println ":: WARNING! The comms went down, going to restart.")
          (stop)
          (<! (async/timeout 3000))
          (recur (inst-comm)))))

    (.join (Thread/currentThread))))

